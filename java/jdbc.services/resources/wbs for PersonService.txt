WBS for CRUD operations in Person Service

WBS for Create method:

/*
Requirement:
    To create person details like person id, email, address_id , birth_date and created_date in person table
    
Entity:
    1.Person
    2.PersonService
    3.AppException
    
Function declaration:
    public long create(Person person)
    
Jobs to be done:
    1) Initialize personId as Zero.
    2) Create addressId and numberOfAffectedRows.
    3) Create a AddressService object.
    4) Check whether first name and last name are not same.
    	4.1) If so, check whether the email is unique.
    		4.1.1) If so, check whether address is given.
    			4.1.1.1) If so, check whether address is already present.
    				4.1.1.1.1) If so, assign the id of the address to addressId.
    				4.1.1.1.2) If not, create a new address and assign the id to addressId.
    			4.1.1.2) If not, assign zero to the addressId.
    		4.1.2) Declare a prepared statement ps as null.
    		4.1.3) Assign the parameter values for prepared statement from the given person.
    		4.1.4) Execute the query and store the value to numberOfAffectedRows.
    		4.1.5) Store the generated id to result set.
    		4.1.6) Get the id from result set and store it to personID.
    		4.1.7) Check whether numberOfAffectedRows or personID is zero.
    			4.1.7.1) If so throw an AppException.
    		4.1.8) Catch any exception if occurs and throw AppException.
    	4.2) If not throw AppException with errorCode EMAIL_NOT_UNIQUE.
    5) If not throw AppException for first name and last name duplicate.
    6) Return personID.
  
Pseudo Code:

	class PersonService {
	
	    public long create(Person person) {
	
	        long personID = 0;
	        long addressId;
	        int numberOfRowsAffected;
	        AddressService addressService = new AddressService();
	
	        if (person.getFirstName().toLowerCase()
	                .equals(person.getLastName().toLowerCase()) != true) {
	
	            if (checkUniqueEmail(0, person.getEmail(), ConnectionService.get()) == true) {
	
	                if (person.getAddress() != null) {
	
	                    if (addressService.checkUniqueAddress(person.getAddress(),
	                            ConnectionService.get()) > 0) {
	                        addressId = addressService.checkUniqueAddress(person.getAddress(),
	                                ConnectionService.get());
	                    } else {
	                        addressId = addressService.create(person.getAddress());
	                    }
	                } else {
	                    addressId = 0;
	                }
	
	                try {
	                    PreparedStatement ps = null;
	                    ps = ConnectionService.get().prepareStatement(
	                            QueryStatement.CREATE_PERSON_QUERY, ps.RETURN_GENERATED_KEYS);
	                    ps.setString(1, person.getFirstName());
	                    ps.setString(2, person.getLastName());
	                    ps.setString(3, person.getEmail());
	                    ps.setDate(4, new java.sql.Date(person.getBirthDate().getTime()));
	                    ps.setLong(5, addressId);
	                    numberOfRowsAffected = ps.executeUpdate();
	                    ResultSet personId = ps.getGeneratedKeys();
	
	                    if (personId.next()) {
	                        personID = personId.getLong("GENERATED_KEY");
	                    }
	                    ps.close();
	
	                    if (numberOfRowsAffected == 0 || personID == 0) {
	                        throw new AppException(ErrorCode.PERSON_CREATION_FAILS);
	                    }
	
	                    ConnectionService.commit();
	
	                } catch (Exception e) {
	                    ConnectionService.rollback();
	                    throw new AppException(ErrorCode.PERSON_CREATION_FAILS, e);
	                }
	            } else {
	                throw new AppException(ErrorCode.EMAIL_NOT_UNIQUE);
	            }
	        } else {
	            throw new AppException(ErrorCode.FIRST_NAME_AND_LAST_NAME_DUPLICATE);
	        }
	
	        return personID;
	    }
	}

*/

WBS For read method:
/*
Requirement:
    To read person details like person id, email, address_id , birth_date and created_date from person table
    
Entity:
    1.Person
    2.PersonService
    3.AppException
    
Function declaration:
    public Person read(long id, boolean addressFlag)

Jobs To Be Done:
    1) Create Person object as person and declare null.
    2) Create an instance for AddressService.
    3) Try to add Query to the prepared statement.
    4) Assign the parameter value for prepared statement.
    5) Execute the query and store the data in result set.
    6) Invoke readPerson method to return the person stored in result set.
    7) Check AddressFlag is true.
    	7.1) If so invoke read method from AddressService.
    8) Catch any exception if occurs and throw AppException.
    9) Return person.
    
Pseudo Code:

	class PersonService {
	
	    public Person read(long id, boolean addressFlag) {
	
	        Person person = null;
	        AddressService addressService = new AddressService();
	
	        try (PreparedStatement ps =
	                ConnectionService.get().prepareStatement(QueryStatement.READ_PERSON_QUERY)) {
	
	            ps.setLong(1, id);
	            ResultSet result = ps.executeQuery();
	
	            while (result.next()) {
	                person = readPerson(result);
	                if (addressFlag) {
	                    person.setAddress(addressService.read(result.getLong("address_id")));
	                }
	            }
	        } catch (Exception e) {
	            throw new AppException(ErrorCode.READING_PERSON_FAILS, e);
	        }
	
	        return person;
	    }
	}


*/

WBS for readall method:
/*
Requirement:
    To read all the person details like person id, email, address_id , birth_date and created_date from person table
    
Entity:
    1.Person
    2.PersonService
    3.AppException
    
Function declaration:
    public ArrayList<Person> readAll()
 
Jobs to be Done:
	1. Create a ArrayList as Persons as type Person.
    2) Create Person object as person and declare null.
    3) Create an instance for AddressService.
    4) Try to add Query to the prepared statement.
    5) Assign the parameter value for prepared statement.
    6) Execute the query and store the data in result set.
    7) Invoke readPerson method to return the person stored in result set.
    8) Invoke address read method to return address and store it to person.
    9) Add person to persons list.
    10) Catch any exception if occurs and throw AppException.
    11) Return persons.
    
	
    
Pseudo Code:

	class PersonService {
	
	    public ArrayList<Person> readAll() {
	
	        ArrayList<Person> persons = new ArrayList<>();
	        AddressService addressService = new AddressService();
	        Person person;
	
	        try (PreparedStatement ps =
	                ConnectionService.get().prepareStatement(QueryStatement.READALL_PERSON_QUERY)) {
	
	            ResultSet result = ps.executeQuery();
	
	            while (result.next()) {
	                person = readPerson(result);
	                Address address = addressService.read(result.getLong("address_id"));
	                person.setAddress(address);
	                persons.add(person);
	            }
	
	        } catch (Exception e) {
	            throw new AppException(ErrorCode.READING_PERSON_FAILS, e);
	        }
	        return persons;
	    }
	}

WBS For update method:

/*
Requirement:
    To update person details like person id, email, address_id , birth_date and created_date from person table
    
Entity:
    1.Person
    2.PersonService
    3.AppException
    
Function declaration:
    public void update(Person person)

Jobs To Be Done:
    1. Declare the variable numberOfRowsAffected as zero 
    2. Checking whether the PostalCode is zero
        2.1) Throw an AppException with message "postal code should not be zero".
    3.Otherwise
      3.1) Create an instance ConnectionService and get connection with the database using jdbc driver.
      3.2) Invoke the init method from ConnectionService using the object jc.
      3.3) Declare a PreparedStatement ps as null.
      3.4) Check whether the email is unique
           3.4.1)Store the updateAddress query from the QueryStatements in the preparedStatement of jc. 
           3.4.2)Invoke the method getStreet from Address and set the value for street.
           3.4.3)Invoke the method getCity from Address and set the value for city.
           3.4.4)Invoke the method getPostalCode from Address and set the value for posal code.. 
           3.4.5)Invoke the method getName from Person and set the value for name.          
           3.4.6)Invoke the method getEmail from Person and set the value for email.
           3.4.7)Invoke the method getBirthDate from Person and set the value for Birthdate.
           3.4.8)Set the value for person id as id.
           3.4.9)Exceute the query in the prepared statement and store it in the numberOfRowsAffected of type long.
           3.4.10) If any exception occurs during updation then throw an exception with the message "SQL exception occured".
           3.4.11) Check whether the numberOfRowsAffected is zero 
                 3.4.11.1)  Then throw AppException with message "updation failed".
                 3.4.11.2)  Otherwise Closing the Jdbc driver connection.
    4. If the email is not unique throw AppException with message "Email already exist".

    
Pseudo Code:

	class PersonService {
	    public void update(Person person) {
	        
	        int numberOfRowsAffected = 0;
	        AddressService addressService = new AddressService();
	        long addressId = 0;
	
	        if (person.getFirstName().toLowerCase()
	                .equals(person.getLastName().toLowerCase()) != true) {
	
	            if (checkUniqueEmail(person.getId(), person.getEmail(),
	                    ConnectionService.get()) == true) {
	
	                if (person.getAddress() == null && addressId(person.getId()) == 0) {
	                    addressId = 0;
	                } else if (person.getAddress() != null && addressId(person.getId()) == 0) {
	
	                    if (addressService.checkUniqueAddress(person.getAddress(),
	                            ConnectionService.get()) > 0) {
	                        addressId = addressService.checkUniqueAddress(person.getAddress(),
	                                ConnectionService.get());
	                    } else {
	                        addressId = addressService.create(person.getAddress());
	                    }
	                } else if ((person.getAddress() != null && addressId(person.getId()) != 0)) {
	
	                    if (addressService.checkUniqueAddress(person.getAddress(),
	                            ConnectionService.get()) > 0) {
	                        addressId = addressService.checkUniqueAddress(person.getAddress(),
	                                ConnectionService.get());
	                    } else {
	                        addressId = addressService.create(person.getAddress());
	                    }
	
	                    if (addressUsage(addressId(person.getId())) != true) {
	                        addressService.delete(addressId(person.getId()));
	                    }
	                } else if ((person.getAddress() == null && addressId(person.getId()) != 0)) {
	                    addressId = addressId(person.getId());
	                }
	
	                try (PreparedStatement ps = ConnectionService.get()
	                        .prepareStatement(QueryStatement.UPDATE_PERSON_QUERY.toString())) {
	
	                    ps.setString(1, person.getFirstName());
	                    ps.setString(2, person.getLastName());
	                    ps.setString(3, person.getEmail());
	                    ps.setLong(4, addressId);
	                    ps.setDate(5, new java.sql.Date(person.getBirthDate().getTime()));
	                    ps.setLong(6, person.getId());
	                    numberOfRowsAffected = ps.executeUpdate();
	
	                    if (numberOfRowsAffected == 0) {
	                        throw new AppException(ErrorCode.PERSON_UPDATION_FAILS);
	                    }
	                    ConnectionService.commit();
	                } catch (Exception e) {
	                    ConnectionService.rollback();
	                    throw new AppException(ErrorCode.PERSON_UPDATION_FAILS, e);
	                }
	            } else {
	                throw new AppException(ErrorCode.EMAIL_NOT_UNIQUE);
	            }
	        } else {
	            throw new AppException(ErrorCode.FIRST_NAME_AND_LAST_NAME_DUPLICATE);
	        }
	    }
	}

*/

WBS For delete method:
/*
Requirement:
    To delete person details like person id, email, address_id , birth_date and created_date from person table
    
Entity:
    1.Person
    2.AppException
    
Function declaration:
    public void delete(long id)

Jobs To Be Done:
    1) Declare numberOfRowsAffected as zero.
    2) Check whether the addressId is not equal to zero.
    	2.1) If so, Check whether the address is not used by other person.
    		2.1.1) If so, delete the address.
    3) Add the query to the prepared statement.
    4) Assign the value of id to the prepared statement parameter.
    5) Execute the query and store the returned value to numberOfAffectedRows.
    6) Check whether the numberOfAffectedRows is zero.
    	6.1) If so throw app exception.
    
Pseudo Code:

	class PersonService {
	    public void delete(long id) {
	
	        int numberOfRowsAffected = 0;
	
	        if (addressId(id) != 0) {
	            AddressService addressService = new AddressService();
	
	            if (addressUsage(addressId(id)) != true) {
	                addressService.delete(addressId(id));
	            }
	        }
	
	        try (PreparedStatement ps =
	                ConnectionService.get().prepareStatement(QueryStatement.DELETE_PERSON_QUERY)) {
	
	            ps.setLong(1, id);
	            numberOfRowsAffected = ps.executeUpdate();
	            ps.close();
	
	            if (numberOfRowsAffected == 0) {
	                throw new AppException(ErrorCode.PERSON_DELETION_FAILS);
	            }
	            ConnectionService.commit();
	
	        } catch (Exception e) {
	            ConnectionService.rollback();
	            throw new AppException(ErrorCode.PERSON_DELETION_FAILS, e);
	        }
	    }
	}
       */

WBS For checkUniqueEmail method:
/*
Requirement:
    To find the email is unique or not.
    
Entity:
    1.Person
    2.AppException
    
Function declaration:
    public boolean checkUniqueEmail(long id, String email, Connection con)

Jobs To Be Done:
    1) Initialize unique field with true.
    2) Try to add email unique query to the prepared statement.
    3) set email as the parameter value.
    4) Execute the query and store the person id to result set.
    5) Get the value of person id from result set and store it to personId.
    6) Check whether id is equal to zero.
    	6.1) If so check whether the personId is greater than zero.
    		6.1.1) If so, set unique as false.
    		6.1.2) If not, set unique as true.
    	6.2) If not, check whether personId is greater than zero and not equal to id.
    		6.2.1) If so, set unique as false.
    		6.2.2) If not, set unique as true.
    7) Catch any exception if occurs and throw AppException.
    8) Return unique.
    
Pseudo Code:

	class PersonService {
	    public boolean checkUniqueEmail(long id, String email, Connection con) {

        ResultSet result;
        boolean unique = true;
        long personId = 0;

        try (PreparedStatement ps = con.prepareStatement(QueryStatement.EMAIL_UNIQUE)) {

            ps.setString(1, email);
            result = ps.executeQuery();

            if (result.next()) {
                personId = result.getLong("id");
            }

            if (id == 0) {
                unique = personId > 0 ? false : true;
            } else {
                unique = (personId > 0 && personId != id) ? false : true;
            }

        } catch (Exception e) {
            throw new AppException(ErrorCode.FAILED_TO_CHECK_EMAIL, e);
        }
        return unique;
    }
	}
*/
       
       
WBS For addressId method:
/*
Requirement:
    To find the address id linked to the given person id.
    
Entity:
    1.Person
    2.AppException
    
Function declaration:
    public long addressId(long id)

Jobs To Be Done:
    1) Declare addressId as zero.
    2) Try to add address id query to the prepared statement.
    3) set person id as the parameter value.
    4) Execute the query and store the person id to result set.
    5) Get the value of address id from result set and store it to addressId.
    6) Catch any exception if occurs and throw AppException.
    7) Return addressId.
    
Pseudo Code:

	class PersonService {
	    public long addressId(long id) {

	        long addressId = 0;
	        try (PreparedStatement ps =
	                ConnectionService.get().prepareStatement(QueryStatement.GET_ADDRESS_ID)) {
	
	            ps.setLong(1, id);
	            ResultSet result = ps.executeQuery();
	
	            if (result.next()) {
	                addressId = result.getLong("address_id");
	            }
	        } catch (Exception e) {
	            throw new AppException(ErrorCode.READING_ADDRESSID_FAILS, e);
	        }
	        return addressId;
	    }
	}
       */
       
WBS For readPerson method:
/*
Requirement:
    To read data from result set and return the data as person.
    
Entity:
    1.Person
    2.AppException
    
Function declaration:
    public Person readPerson(ResultSet result)

Jobs To Be Done:
    1) Initialize person object as null.
    2) Try to read each result column and store it to person.
    3) Catch any exception if occurs and throw AppException.
    4) return person.
    
Pseudo Code:

	class PersonService {
	    public Person readPerson(ResultSet result) {

	        Person person = null;
	
	        try {
	            person = new Person(result.getString("first_name"), result.getString("last_name"),
	                    result.getString("email"), new java.util.Date(result.getDate("birth_date").getTime()));
	            person.setCreatedDate(result.getTimestamp("created_date"));
	            person.setId(result.getLong("id"));
	        } catch (Exception e) {
	            throw new AppException(ErrorCode.READING_PERSON_FAILS, e);
	        }
	
	        return person;
	    }
	}
 */
 
 WBS For addressUsage method:
/*
Requirement:
    To find whether the address is used by other persons or not.
    
Entity:
    1.Person
    2.AppException
    
Function declaration:
    public boolean addressUsage(long addressId)

Jobs To Be Done:
    1) Initialize usage as true.
    2) Try to add address usage query to prepared statement.
    3) Set the address id as the prepared statement parameter value.
    4) Store the returned count to result set.
    5) Get the count from result set and store it to count.
    6) Check whether the count id greater than 1.
    	6.1) If so set usage as true.
    	6.2) If count is equal to set usage as false.
   	7) Catch any exception if occurs and throw AppException.
   	8) Return usage.
    
Pseudo Code:

	class PersonService {
	    public boolean addressUsage(long addressId) {

	        boolean usage = true;
	
	        try (PreparedStatement ps =
	                ConnectionService.get().prepareStatement(QueryStatement.ADDRESS_USAGE)) {
	            ps.setLong(1, addressId);
	            ResultSet result = ps.executeQuery();
	
	            if (result.next()) {
	                int count = result.getInt("COUNT(person.id)");
	
	                if (count > 1) {
	                    usage = true;
	                } else if (count == 1) {
	                    usage = false;
	                }
	            }
	        } catch (Exception e) {
	            throw new AppException(ErrorCode.CHECKING_ADDRESS_USAGE_FAILS, e);
	        }
	
	        return usage;
	    }
	}
 */
 
  WBS For addressUsage method:
/*
Requirement:
    To format the date of birth property.
    
Entity:
    1.Person
    2.AppException
    
Function declaration:
    public java.util.Date dateValidator(String date)

Jobs To Be Done:
    1) Initialize usage as true.
    2) Try to add address usage query to prepared statement.
    3) Set the address id as the prepared statement parameter value.
    4) Store the returned count to result set.
    5) Get the count from result set and store it to count.
    6) Check whether the count id greater than 1.
    	6.1) If so set usage as true.
    	6.2) If count is equal to set usage as false.
   	7) Catch any exception if occurs and throw AppException.
   	8) Return usage.
    
Pseudo Code:

	class PersonService {
	    public java.util.Date dateValidator(String date) {
	        SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
	        java.util.Date utilDate = null;
	        boolean valid = false;
	        
	        try {
	            LocalDate.parse(date,
	                    DateTimeFormatter.ofPattern("dd-MM-uuuu")
	                            .withResolverStyle(ResolverStyle.STRICT));
	            
	            valid = true;
	        } catch (Exception e) {
	            valid = false;
	            throw new AppException(ErrorCode.WRONG_DATE_FORMAT, e);
	        }
	        
	        if (valid == true) {
	            try {
	                utilDate = formatter.parse(date);
	            } catch (Exception e) {
	                throw new AppException(ErrorCode.WRONG_DATE_FORMAT, e);
	            }
	        }
	        return utilDate;
	    }
	}
 */