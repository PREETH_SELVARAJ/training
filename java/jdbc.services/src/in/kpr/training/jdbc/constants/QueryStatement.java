package in.kpr.training.jdbc.constants;

public class QueryStatement {

    public static final String CREATE_ADDRESS_QUERY     = new StringBuilder()
            .append("INSERT INTO address (street, city, postal_code)")
            .append("     VALUES (?, ?, ?)                          ")
            .toString();

    public static final String READ_ADDRESS_QUERY       = new StringBuilder()
            .append("SELECT address.id         ")
            .append("      ,address.street     ")
            .append("      ,address.city       ")
            .append("      ,address.postal_code")
            .append("  FROM address            ")
            .append(" WHERE id = ?             ")
            .toString();

    public static final String READALL_ADDRESS_QUERY    = new StringBuilder()
            .append("SELECT address.id         ")
            .append("      ,address.street     ")
            .append("      ,address.city       ")
            .append("      ,address.postal_code")
            .append("  FROM details.address    ")
            .toString();
    
    public static final String UPDATE_ADDRESS_QUERY     = new StringBuilder()
            .append("UPDATE address         ")
            .append("   SET street = ?      ")
            .append("      ,city = ?        ")
            .append("      ,postal_code = ? ")
            .append(" WHERE id = ?          ")
            .toString();
    
    public static final String DELETE_ADDRESS_QUERY     = new StringBuilder()
            .append("DELETE FROM address")
            .append(" WHERE id=?        ")
            .toString();

    public static final String CREATE_PERSON_QUERY      = new StringBuilder()
            .append("INSERT INTO person (first_name, last_name, email, birth_date, address_id)")
    		.append("     VALUES (?, ?, ?, ?, ?)                                              ")
    		.toString();

    public static final String READ_PERSON_QUERY        = new StringBuilder()
            .append("SELECT person.id           ")
            .append("      ,person.first_name   ")
            .append("      ,person.last_name    ")
            .append("      ,person.email        ")
            .append("      ,person.address_id   ")
            .append("      ,person.birth_date   ")
            .append("      ,person.created_date ")
            .append(" FROM person               ")
            .append("WHERE id = ?               ")
            .toString();

    public static final String READALL_PERSON_QUERY     = new StringBuilder()
            .append("SELECT person.id           ")
            .append("      ,person.first_name   ")
            .append("      ,person.last_name    ")
            .append("      ,person.email        ")
            .append("      ,person.address_id   ")
            .append("      ,person.birth_date   ")
            .append("      ,person.created_date ")
            .append("  FROM person              ")
            .toString();

    public static final String UPDATE_PERSON_QUERY      = new StringBuilder()
            .append("UPDATE person        ")
            .append("   SET first_name = ?")
            .append("      ,last_name = ? ")
            .append("      ,email = ?     ")
            .append("      ,address_id = ?")
            .append("      ,birth_date = ?")
            .append(" WHERE id = ?        ")
            .toString();
    
    public static final String DELETE_PERSON_QUERY      = new StringBuilder()
            .append("DELETE FROM person")
            .append(" WHERE id=?       ")
            .toString();

    public static final String EMAIL_UNIQUE             = new StringBuilder()
            .append("SELECT person.id       ")
            .append("  FROM person          ")
            .append(" WHERE person.email = ?")
            .toString();
    
    public static final String GET_ADDRESS_ID           = new StringBuilder()
            .append("SELECT person.address_id ")
            .append("  FROM person            ")
            .append(" WHERE person.id = ?     ")
            .toString();
    
    public static final String ADDRESS_UNIQUE           = new StringBuilder()
            .append("SELECT address.id                 ")
            .append("  FROM address                    ")
            .append(" WHERE address.street = ?         ")
            .append("       AND address.city = ?       ")
            .append("       AND address.postal_code = ?")
            .toString();
    
    public static final String ADDRESS_SEARCH           = new StringBuilder()
            .append("SELECT id              ")
            .append("      ,street          ")
            .append("      ,city            ")
            .append("      ,postal_code     ")
            .append("  FROM address         ")
            .append(" WHERE street          ")
            .append("       LIKE %?%        ")
            .append("       AND city        ")
            .append("       LIKE %?%        ")
            .append("       AND postal_code ")
            .append("       LIKE %?%        ")
            .toString();
    
    public static final String ADDRESS_TABLE_SIZE       = new StringBuilder()
            .append("SELECT COUNT(*) ")
            .append("  FROM address  ")
            .toString();

    public static final String PERSON_TABLE_SIZE        = new StringBuilder()
            .append("SELECT COUNT(*) ")
            .append("  FROM person   ")
            .toString();
    
    public static final String ADDRESS_USAGE            = new StringBuilder()
            .append("SELECT COUNT(person.id)     ")
            .append("  FROM person               ")
            .append(" WHERE person.address_id = ?")
            .toString();
    
    public static final String NAME_UNIQUE              = new StringBuilder()
            .append("SELECT person.id               ")
            .append("  FROM person                  ")
            .append(" WHERE person.first_name = ?   ")
            .append("       AND person.last_name = ?")
            .toString();
}