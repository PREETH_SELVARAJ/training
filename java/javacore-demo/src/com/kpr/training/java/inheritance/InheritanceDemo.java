/*
Requirement:
    Demonstrate inheritance, overloading, overriding using Animal, Dog, Cat and Snake class 
    of objects

Entity:
    InheritanceDemo
    Dog
    Animal
    cat
    snake
    
Function Declaration:
    public void move()
    public void DogType()
    Public void move(int km)
    
Jobs To Be Done:
    1)Create a object for the Animal as animal.
    2)Create a object for the Dog as dog.
    3)Create a object for the Cat as cat.
    4)Create a object for the Snake as snake.
    5)Refer a Dog type animal1 to Animal type.
    6)Invoke the method for the object animal.
      6.1)Print as "The animal is moving" 
    7)Invoke the method for the object dog.
      7.1)Print as "Dog is moving" .
    8)Invoke the method for the object dog and pass 2 as parameter.
      8.1)Print as "The dog moves 2 km" .
    9)Invoke the method for the object animal1.
      9.1)Print as "Dog is moving".
*/

package com.kpr.training.java.inheritance;

public class InheritanceDemo {
    
    public static void main(String[] args) {
    
        Animal animal = new Animal();
        Dog dog = new Dog();
        Cat cat = new Cat();
        Snake snake = new Snake();
        Animal animal1 = new Dog();
        animal.move();
        dog.move();
        dog.move(2);
        animal1.move();
    }
}
