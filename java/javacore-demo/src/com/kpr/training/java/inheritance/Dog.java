package com.kpr.training.java.inheritance;

public class Dog extends Animal {
    
    public void DogType() {    
        System.out.println("Dog type:Rottweiler");
    }
    
    public void move() {    
        System.out.println("Dog is moving");
    }
    //method overloading
    public void move(int km) {  
        System.out.println("The dog moves " + km +" km");
    }       
}

