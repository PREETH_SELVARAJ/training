/*
Requirement:What methods would a class that implements the java.lang.CharSequence interface have to 
            implement?

ANSWER:
charAt, length, subSequence and toString.

----------------------------------------------------------------------------------------------------


Requirement:Is the following interface valid?
            public interface Marker {}

ANSWER: 
Yes. Methods are not required. Empty interfaces can be used as types and to mark classes 
without requiring any particular method implementations. For an example of a useful empty interface,
see java.io.Serializable.

----------------------------------------------------------------------------------------------------
*/
