/*
Requirement:
    To answer the following question.
      String hannah = "Did Hannah see bees? Hannah did.";
      - What is the value displayed by the expression hannah.length()?
      - What is the value returned by the method call hannah.charAt(12)?
      - Write an expression that refers to the letter b in the string referred to by hannah.

Entity:
    StringFunction 

Function Declaration:
    public static void main(String[]  args)

Jobs to be done:
    1)string named hannah is declared and a value is stored in it.
    2)The length of the given string is printed using length function.
    3)Print the character at the 12th position.
    4)Print the character at the 15th position.
*/  
/*SOLUTION:
-32
-e
-hannah.charAt(15)
*/

package com.kpr.training.java.string;

import java.lang.String;

public class StringFunction {
    
    public static void main(String[] args) { 
  
        String hannah = "Did Hannah see bees? Hannah did.";
        System.out.println(hannah.length());//OUTPUT : 32 
        System.out.println(hannah.charAt(12));//OUTPUT : e 
        System.out.println(hannah.charAt(15));//OUTPUT : b
    }
}
