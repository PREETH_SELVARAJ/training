/*
Requirement:
    How long is the string returned by the following expression? What is the string?
    "Was it a car or a cat I saw?".substring(9, 12)

Entity:
    SubStringDemo

Function Declaration:
    public static void main(String[] args)

Jobs To Be Done:
    1)string of type string has been declared
    2)"Was it a car or a cat I saw" in string.
    3)Then the substring between 9 and 12 is stored in substring.
    4)subString is printed.
*/
/*solution:cat
*/

package com.kpr.training.java.string;

import java.lang.String;

public class SubStringDemo {
    
    public static void main(String[] args) {
        
        String string;
        string = "Was it a car or a cat I saw?" ;
        String subString = string.substring(9,12) ;
        System.out.println("substring:" + subString );//ouput:car
    }
}