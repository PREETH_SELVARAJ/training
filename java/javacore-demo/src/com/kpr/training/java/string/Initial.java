/*
Requirement:
    To print the initial from the full name

Entity:
    Initial

Function Declaration:
    public static void main(String[] args)
    
Jobs To Be Done:
    1)Declare name as string and Isha in it.
    2)Find the length of the name and store in length.
    3)For each Character
      3.1)Check whether the character is in upper character
         3.1.1)Print the character.
*/

package com.kpr.training.java.string;

public class Initial {
    
    public static void main(String[] args) {
        
        
        String name = "Isha" ;
        int length = name.length();
        System.out.print("Initial:");
        for (int i = 0; i < length; i++) {
            
            if (Character.isUpperCase(name.charAt(i))) {
                System.out.print(name.charAt(i)+" ") ;
            }
        }
    }
}