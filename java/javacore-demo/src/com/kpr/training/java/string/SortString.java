/*
Requirement:
    To sort string[] alphabetically ignoring case and to print even index indexed into 
    uppercase
    
Entity:
   SortString

Function Declaration:
    public static void main(String[] args)

Jobs To Be Done:
    1)Create an String array named array and store the given cities.
    2)Sort the array ignoring cases.
    3)print the sorted array.
    4)For each index of the array
      4.1)Check whether the index is even
          4.1.1)If the condition is true then change the element at the particular index to upper
          case.
    5)Print the array.
 */

package com.kpr.training.java.string;

import java.util.Arrays;

class SortString {
    
    public static void main(String[] args) {
      
        String[] array={ "Madurai", "Thanjavur", "TRICHY", "Karur", "Erode", "trichy", "Salem" };
        Arrays.sort(array,String.CASE_INSENSITIVE_ORDER);
        System.out.print("sorted list:");
        System.out.println(Arrays.asList(array));
        for(int index = 0; index < array.length; index++) {
           
            if(index % 2 == 0) {    
               array[index] = array[index].toUpperCase();
            } 
        } 
        System.out.println("even index in uppercase:");
        System.out.println(Arrays.asList(array));
    }
}