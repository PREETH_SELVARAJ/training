/*
Requirement:
   Show two ways to concatenate the following two strings together to get the string "Hi, mom.":
       String hi = "Hi, ";
       String mom = "mom.";
Entity:
    Concatenate
Function Declaration: 
    public static void main(String[] args)
Jobs To Be Done:
    1)Declare hi as type String and store "Hi, " in it .
    2)Declare mom as type String and store "mom. " in it.
    3)Add hi and mom in concatenatedString and declare s as string.
    4)Print concatenatedString
    5)Add mom to hi using method concat and store in concatenatedString1
    6)print the concatenatedString1.
    
*/

package com.kpr.training.java.string;

public class Concatenate {
    
    public static void main(String[] args) {
        
        String hi = "Hi, ";
        String mom = "mom.";
        String  concatenatedString = hi + mom ;
        System.out.println("concatenated string:"+concatenatedString);
        String concatenatedString1 = hi.concat(mom);
        System.out.println("concatenated string:"+concatenatedString1);
    }
}

    