/*
Requirement:To state why the number 6 is printed twice in a row
            class PrePostDemo {
                public static void main(String[] args) {
                    int i = 3;
                    i++;
                    System.out.println(i);    // "4"
                    ++i;
                    System.out.println(i);    // "5"
                    System.out.println(++i);  // "6"
                    System.out.println(i++);  // "6"
                    System.out.println(i);    // "7"
                }
            }
Entities:PrePostDemo
Function Declaration:No function declaration
Jobs to be done:check for the post increment and pre increment operator and the reason for 
                displaying 6 twice is stated.
*/
/*
In the code System.out.println(++i),++i is an pre increment operator so that 5 is incremented to 6
and in the next line System.out.println(i++), i++ is a post increment operator where the current 
value i.e 6 is printed and then it is increased to by one i.e 7 so that 6 is printed twice.
*/