/*
Requirement:
    To print the Class names of the primitive datatype.

Entity:
    PrimitiveClassName

Function declaration:
    public static void main(String[] args)
    getName()
    
Jobs to be done:
    1)Declare a variable className of type string.
    2)Then store the class name of integer className.
    3)Print the className
    4)Store the class name of char className.
    5)Print the className.
    6)Then store the class name of double className.
    7)Print the className .
    8)Then store the class name of double className.
    9)Print the className.
*/

package com.kpr.training.java.datatype;

public class PrimitiveClassName {
    
    public static void main(String[] args) {
        
        String className ;
        className = int.class.getName();
        System.out.println("Class Name of Int : " + className);
        className = char.class.getName();
        System.out.println("Class Name of Char : " + className);
        className = double.class.getName();
        System.out.println("Class Name of Double : " + className);
        className = float.class.getName();
        System.out.println("Class Name of Float : " + className);
    }
}
