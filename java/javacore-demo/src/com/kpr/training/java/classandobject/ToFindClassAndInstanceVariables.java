package com.kpr.training.java.classandobject;
/*
Requirement:
    To find the class variable and instance variable for the program given
        public class IdentifyMyParts {
        public static int x = 7;
        public int y = 3;
        }
  
Entities:
    identifyMyParts

Function Declaration:
    No function declaration in this program

Jobs to be done:
    1)Find the class variable which has been declared static   
    2)Find the instance variable  which are non-static variable
*/
/*class varaible:x
instance varaiable:y                  
*/

