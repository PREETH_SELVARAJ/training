/*
Requirement:
      Change the following program to use compound assignments:
       class ArithmeticDemo {

            public static void main (String[] args){

                 int result = 1 + 2; // result is now 3
                 System.out.println(result);

                 result = result - 1; // result is now 2
                 System.out.println(result);

                 result = result * 2; // result is now 4
                 System.out.println(result);

                 result = result / 2; // result is now 2
                 System.out.println(result);

                 result = result + 8; // result is now 10
                 result = result % 7; // result is now 3
                 System.out.println(result);
            }
       }

Entity:
    ArithmeticDemo

Function Declaration:
    public static void main (String[] args)

Jobs to be done:
    1)1 and 2 are summed up and stored in result.
    2)result is printed.
    3)result is decremented by one and stored in result.
    4)result is printed.
    5) result is multiplied by two and stored in result .
    6)result is printed.
    7)result is divided by two and stored in result .
    8)result is printed.
    9)result is incremented by eight and stored in result .
    10)result is printed.
    11)1Modulo operation by 7 to the result is done and stored .
    12)result is printed.
*/

package com.kpr.training.java.classandobject;

 class ArithmeticDemo {

    public static void main (String[] args ) {
        
        int result = 1 + 2; // result is now 3
        System.out.println(result);

        result -= 1; // result is now 2
        System.out.println(result);

        result *= 2; // result is now 4
        System.out.println(result);

        result /= 2; // result is now 2
        System.out.println(result);

        result += 8; // result is now 10
        System.out.println(result);
        result %= 7; // result is now 3
        System.out.println(result);
    }
}

