package com.kpr.training.bank.people;

import com.kpr.training.bank.branch.Branch;

public class Manager extends People {
	
	public String name = "abi";
	public int experience = 5;
	
	public static void main(String[] args) { 
		
		Manager manager = new Manager();
		Branch branch = new Branch(); 
		System.out.println(manager.name);
		System.out.println(manager.experience);
		System.out.println(branch.branchLocation);
	}

}
