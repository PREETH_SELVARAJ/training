/*
Requirement:
    Code a functional program that can return the sum of elements of varArgs, passed into the 
method of functional interface


Entity:
    VarargDemo
    VarargInterface

Function Declaration:
    public int add(int ... numbers)
    public static void main(String[] args)
    
Jobs To Be Done:
    1)Create an object varardInterface for interface VarardDemo 
      1.1)Declare the variable sum as type int and store 0 in it.
      1.2)for each number 
          1.2.1)Add sum and the number and store in sum.
      1.3)Return sum.
      2)Invoke the method add of the interface VarargInterface and print the sum of the varargs. 

pseudo code:

interface VarargInterface {
    
    public int add(int ... numbers);
}
public class VarargDemo {
    
    public static void main(String[] args) {
       
    	VarargInterface value = (numbers) -> {
           //return sum 
        };
        System.out.println("sum : " + value.add(10,10,10,10));
    }
}
*/

package com.kpr.training.lambda;

interface VarargInterface {
    
    public int add(int ... numbers);
}
public class VarargDemo {
    
    public static void main(String[] args) {
       
    	VarargInterface value = (numbers) -> {
            int sum = 0;
            for(int number : numbers) {
                sum = sum + number;
            }
            return sum;
        };
        System.out.println("sum : " + value.add(10,10,10,10));
    }
}
