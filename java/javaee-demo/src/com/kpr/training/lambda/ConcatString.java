/* Requirement:
 *     write a Lambda expression program with a single method interface
 *     To concatenate two strings.
 * Entity:
 *     LambdaInterfaceConcat
 *     ConcatString
 * Function Declaration:
 *     public static void main(String[] args)
 *     public String concatString(String string1 , String string2);
 * Jobs To Be Done:
 *     1)Create an object for the interface LambdaInterfaceConcat name lambdaInterfaceConcat
 *       1.1)Returns the concatenation of two strings.
 *     2)Invoke the method concat of LambdaInterfaceConcat
 *       2.1)Print the concatenated string.
 *     
 *pseudo code:
    interface LambdaInterfaceConcat {
    
       String concat(String string1 , String string2);
    
    }
    public class ConcatString {
    
    	static LambdaInterfaceConcat lambdaInterfaceConcat = (string1, string2) -> {
    		return string1 + string2 ;
    	};
    
    	public static void main(String[] args) {	
    			System.out.println(lambdaInterfaceConcat.concat("BALL ", "BADMINTON"));	        
    	}
    

 */

package com.kpr.training.lambda;

interface LambdaInterfaceConcat {

    String concat(String string1 , String string2);
}

public class ConcatString {
	
	static LambdaInterfaceConcat lambdaInterfaceConcat = (string1, string2) -> {
		return string1 + string2 ;
	};
	
	 public static void main(String[] args) {	
			System.out.println(lambdaInterfaceConcat.concat("BALL ", "BADMINTON"));	        
	    }
}

