/*
 * Requirements : 
 *       To find what's wrong with the following program? And fix it using Type Reference

    public interface BiFunction{
        int print(int number1, int number2);
    }
    
    public class TypeInferenceExercise {
        public static void main(String[] args) {
    
            BiFunction function = (int number1, int number2) ->  { 
            return number1 + number2;
            };
            
            int print = function.print(int 23,int 32);
            
            System.out.println(print);
        }
    }
    
 * Entities :
 *     interface BiFunction,
 *     public class TypeInferenceExercise.
 *     
 * Function Declaration :
 *     int print(int number1, int number2),
 *     public static void main(String[] args).
 * 
/*
 * The interface is declared as public and in print method the data type is already defined. 
 */
package com.kpr.training.lambda;


interface BiFunction{ // public is removed
    int print(int number1, int number2);
}

public class TypeInferenceDemo {
    public static void main(String[] args) {

        BiFunction function = (int number1, int number2) ->  { 
        return number1 + number2;
        };
        
        int print = function.print(23, 32);// datatype is removed.
        
        System.out.println(print);
    }
}