/*
 * Requirements : 
 *       To convert the following anonymous class into lambda expression.
 *  interface CheckNumber {
        public boolean isEven(int value);
    }
    
    public class EvenOrOdd {
       
        public static void main(String[] args) {
            CheckNumber number = new CheckNumber() {
                public boolean isEven(int value) {
                    if (value % 2 == 0) {
                        return true;
                    } else return false;
                }
            };
            System.out.println(number.isEven(11));
        }
    }
 * Entities :
 *     interface CheckNumber,
 *     public class EvenOrOdd.
 *     
 * Function Declaration :
 *     boolean isEven(int number);
 *     public static void main(String[] args).
 *     
 * Jobs To Be Done:
 *     1)Creating a object for the Scanner named scanner
 *     2)A variable is declared of type integer named num.
 *     3)The user is prompted to enter the value which will be stored in num.
 *     4)Creating check reference for CheckNUmber interface
 *         4.1)Returns true if the number is divisible by two.
 *         4.2)Otherwise it will return false.
 *     5)Invoke the method isEven and print the returned value from it.
 *              
 */
package com.kpr.training.lambda;


import java.util.Scanner;

interface CheckNumber {
    
    boolean isEven(int number);
    
}

public class EvenOrOdd {

    public static void main(String[] args) {
        
        Scanner scanner = new Scanner(System.in);
        System.out.print("Enter the number:");
        int num = scanner.nextInt();
        CheckNumber check = (int number) -> {
        	return (number % 2 == 0) ;
        };
        System.out.println(check.isEven(num));
        scanner.close();
    }
}