/* Requirement:
 *     write a program to print difference of two numbers using lambda expression 
 *     and the single method interface
 * Entity:
 *     LambdaInterfaceDifference
 *     Difference
 * Function Declaration:
 *     public static void main(String[] args)
 *     public int difference(int number1 , int number2);
 * Jobs To Be Done:
 * 	   1)Create an object for the interface LambdaInterfaceDifference name lambdaInterfaceDifference
 *       1.1)Returns the difference between two numbers.
 *     2)Invoke the method difference of LambdaInterfaceDifference
 *       2.1)Print the difference between the two numbers.
 *     
 *pseudo code:
 *		interface LambdaInterfaceConcat{

                 String concat(String string1 , String string2);
         }

         public class ConcatString {
	            //returns the (number1- number2) using lambda operation.
	              
	            public static void main(String[] args) {	
			             System.out.println(lambdaInterfaceDifference.difference(10,5));	        
	            }
         }
 *
 */
package com.kpr.training.lambda;

interface LambdaInterfaceDifference {
	
	      public int difference(int number1 , int number2);
}

public class Difference {
	
	static LambdaInterfaceDifference lambdaInterfaceDifference = (number1 , number2) -> { 
	    return number1 - number2 ;
    } ;
	
public static void main(String[] args) {		
		System.out.println(lambdaInterfaceDifference.difference(10,8));
	}
}

