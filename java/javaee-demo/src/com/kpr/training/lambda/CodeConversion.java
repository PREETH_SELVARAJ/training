/*

 * Requirements : 
 *      Convert this following code into a simple lambda expression
        int getValue(){
            return 5;
        }
 *Entities :
 *    interface Value,
 *    public class LambdaConvert
 *    
 *Function Declaration :
 *    int getValue();
 *    public static void main(String[] args).
 *    
 *Jobs To Be Done:
 *    1)Creating a Value interface and create a getValue method.
 *    2)Inside the  main method,Create a test reference and defining a lambda function to return 5.
 *    3)Printing the result using getValue method.
 *                  
 *pseudo code:
 	interface Value {
    
    	int getValue();
	}

	public class CodeConversion {
	
		public static void main(String[] args) {
       
			Value test = () -> 5;
        	System.out.println(test.getValue());
    	}
	}
 */


package com.kpr.training.lambda;

interface Value {
    
    int getValue();
}

public class CodeConversion {
	
	public static void main(String[] args) {
       
		Value test = () -> 5;
        System.out.println(test.getValue());
    }
}
