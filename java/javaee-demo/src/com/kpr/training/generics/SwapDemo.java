/*
Requirement :
    Write a generic method to exchange the positions of two different elements in an array.

Entity:
    SwapDemo

Function Declaration:
    public static <T> void swap(T[] list, int firstNumber, int lastNumber)
    public static <T> void printSwap(List<T> list1, int firstNumber, int secondNumber)
    public static void main(String[] args)

Jobs to be done:
    1)Create an Array named array.
    2)Print the list.
    3)Invoke the method swap
       3.1)Declare a variable temporary of genric type T and store the number at the first index.
       3.2)Store number at the second index to first index
       3.3)Store the temporary value to the first index.
    4)Print the array.    
    
Pseudo code:
public class SwapDemo {
	
    public static <T> void swap(T[] number, int number1, int number2) {
    	
        T temporary = number[number1];
        number[number1] = number[number2];
        number[number2] = temporary;
    }

    public static void main(String[] args) {
    	
        Integer[] list = {1, 2, 3, 4, 5, 6};

        System.out.print("\t" + Arrays.toString(list));
        
        swap(list, 0, 5);
        
        System.out.print("\t" + Arrays.toString(list));
        
    }
}
	
*/

package com.kpr.training.generics;

import java.util.Arrays;

public class SwapDemo {
	
    public static <T> void swap(T[] number, int number1, int number2) {
    	
        T temporary = number[number1];
        number[number1] = number[number2];
        number[number2] = temporary;
    }

    public static void main(String[] args) {
    	
        Integer[] list = {1, 2, 3, 4, 5, 6};
        
        System.out.print("Before Swapping : ");
        System.out.print("\t" + Arrays.toString(list));
        
        swap(list, 0, 5);
        
        System.out.print("\nAfter Swapping : ");
        System.out.print("\t" + Arrays.toString(list));
        
    }
}