/*
Requirement :
    Write a program to demonstrate generic - for loop, for list, set and map

Entity:
    GenericLoopDemo
 
Function Declaration:
    add();
    getKey();
    setKey();
    public static void main(String[] args );
    
Jobs To be Done :
    1)Create a HashSet named set
    2)Add the values to the set
    3)For each element in the set
      3.1)Print the element.
    4)Create a ArrayList named list
    5)Add the values to the list.
    6)For each element in the list
      6.1)Print the element.
    7)Create a HashMap named map
    8)Add the elements to the list
    9)For each element in the list
      9.1)Print the element.

pseudo code:

public class GenericLoop {
    
    public static void main(String[] args) {
        
        Set<String> set = new HashSet<>();
        //Add elements to the set
        for (String words : set) {
            System.out.println("the words are " + words);
        }

        List<Integer> list = new ArrayList<>();
        //Add elements to the list
        for (Integer number : list) {
            System.out.println("the numbers are " + number);
        }

        HashMap<Integer , String> map = new HashMap<>();
        //Add elements to the map
        for (Integer i : map.keySet()) {
            System.out.println("key: " + i + " value: " + map.get(i));
        }
    }
}
 */

package com.kpr.training.generics;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class GenericLoop {
    
    public static void main(String[] args) {
        
        //set using for each loop
        Set<String> set = new HashSet<>();
        set.add("Isha");
        set.add("Sri");
        set.add("Sid");
        for (String words : set) {
            System.out.println("the words are " + words);
        }
        
        //list using for each loop
        List<Integer> list = new ArrayList<>();
        list.add(11);
        list.add(12);
        list.add(13);
        list.add(14);
        for (Integer number : list) {
            System.out.println("the numbers are " + number);
        }
        
        //map using for each loop
        HashMap<Integer , String> map = new HashMap<>();
        map.put(1, "One");
        map.put(2, "Two");
        map.put(3, "Three");
        map.put(4, "Four");
        for (Integer i : map.keySet()) {
            System.out.println("key: " + i + " value: " + map.get(i));
        }
    }
}