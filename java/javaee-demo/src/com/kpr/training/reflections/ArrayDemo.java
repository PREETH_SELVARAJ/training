/*
Requirement:
    To code for creating java array using reflect array class and add elements to it.
    Retrieve the added elements using the appropriate method.
    
Entity:
    ArrayReflectionDemo
    
Function Declaration:
    public static void main(String[] args)
    
Jobs to be done:
    1. Create a integer variable named sizeOfArray and store 3 in it.
    2. Create a variable array of type integer array and that stores the array created.
    3. Add the elements to it 
    4. Print the array.
    5. For each element 
       5.1. Print the element.
  
    
Pseudo code:
public class ArrayDemo {
    
    public static void main(String[] args) {
        int sizeOfArray = 3;
        
        // creating integer array using reflection
        int[] array = (int[]) Array.newInstance(int.class, sizeOfArray);
        
        // adding elements in intArray
        Array.setInt(array, 0, 12);
        Array.setInt(array, 1, 14);
        Array.setInt(array, 2, 15);
        
        // printing elements
        System.out.println("The integer array is: " + Arrays.toString(array));
        
       //retrieve the elements
        for (int iteration = 0; iteration < sizeOfArray; iteration++) {
            System.out.println("The elements at index " + iteration + ":" 
                                                        + Array.getInt(array, iteration));
         }
    }
}
*/
package com.kpr.training.reflections;

import java.lang.reflect.Array;
import java.util.Arrays;

public class ArrayDemo {
    
    public static void main(String[] args) {
        int sizeOfArray = 3;
        
        // creating integer array using reflection
        int[] array = (int[]) Array.newInstance(int.class, sizeOfArray);
        
        // adding elements in intArray
        Array.setInt(array, 0, 12);
        Array.setInt(array, 1, 14);
        Array.setInt(array, 2, 15);
        
        // printing elements
        System.out.println("The integer array is: " + Arrays.toString(array));
        
        //retrieve the elements
        for (int iteration = 0; iteration < sizeOfArray; iteration++) {
            System.out.println("The elements at index " + iteration + ":" 
                                                        + Array.getInt(array, iteration));
        }
    }
}