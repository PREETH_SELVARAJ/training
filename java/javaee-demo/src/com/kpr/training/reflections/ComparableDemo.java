package com.kpr.training.reflections;

//class Node<T> implements Comparable<T> {
//    public int compareTo(T obj) { /* ... */ //}
          // ...
//}
/*
Will the following code compile? If not, why?

Answer: Yes.
Node<String> node = new Node<>();
Comparable<String> comp = node;
*/