/*
Requirement:
     sort the roster list based on the person's age in descending order using comparator
     
Entity:
     SortComparator
     Person
     
Function Signature :
     public static void main(String[] args) {}
     
Jobs To Be Done:
     1) Create an object as roster for the method createRoster().
     2) Sort the roster in descending order using comparator.
     3) Convert the roster to stream.
     4) Print all the person details using for each loop.
     
Pseudo code:
	class SortComparator {
	
		public static void main(String[] args) {
		    List<Person> roster = Person.createRoster();
		    //sort the roster using comparator
		    roster.sort(Comparator.comparing(Person::getAge));
		    Stream<Person> stream = roster.stream();
		    stream.forEach(person -> System.out.println(person.name + " " + person.birthday + " "
		            + person.gender + " " + person.emailAddress));
		}
	}
*/

package com.kpr.training.stream;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Stream;

public class DescendingOrder {

    public static void main(String[] args) {
    	
        List<Person> roster = Person.createRoster();
        
        roster.sort(Comparator.comparing(person -> person.getAge()));
        
        Stream<Person> stream = roster.stream();
        stream.forEach(person -> System.out.println(person.getAge() + "\t" + person.name + "\t" + person.birthday + "\t"
                + person.gender + "\t" + person.emailAddress));
    }
}