/*
 * Requirement:
 *    Consider a following code snippet:
          List<Integer> randomNumbers = Array.asList({1, 6, 10, 25, 78})
          - Find the sum of all the numbers in the list using java.util.Stream API
          - Find the maximum of all the numbers in the list using java.util.Stream API
          - Find the minimum of all the numbers in the list using java.util.Stream API 
          
 * Entity:
 *    ListOperationDemo
 * 
 * Function Declaration:
 *    public static void main(String[] args) 
 *    
 * Jobs To Be Done:
 *    1)Create the list of type Integer named randomNumbers
 *    2)Print the list .
 *    3)Store the sum of the elements in the randomNumbers in sum.
 *    4)Print the sum
 *    5)Store the maximum number  in the randomNumbers in maximumNumber.
 *    6)Print the maximumNumber.
 *    7)Store the minimum number  in the randomNumbers in minimumNumber.
 *    8)Print the minimumNumber.
 *    
 * Pseudo code:
 public class ListOperationDemo {
    
    public static void main(String[] args) {
        
    	List<Integer> randomNumbers = Arrays.asList(1, 6, 10, 25, 78);
        System.out.println(randomNumbers);
        int sum = randomNumbers.stream().mapToInt(Integer::valueOf).sum();
        System.out.println(sum);
        int maximumNumber = randomNumbers.stream().max(Integer::compare).get();
        System.out.println(maximumNumber);
        int minimumNumber = randomNumbers.stream().min(Integer::compare).get();
        System.out.println(minimumNumber);
    }
}
 * 
 */
package com.kpr.training.stream;

import java.util.Arrays;
import java.util.List;

public class ListOperationDemo {
    
    public static void main(String[] args) {
        
    	List<Integer> randomNumbers = Arrays.asList(1, 6, 10, 25, 78);
        System.out.println("List elements are " + randomNumbers);
        int sum = randomNumbers.stream().mapToInt(Integer::valueOf).sum();
        System.out.println("Sum of the list elements " + sum);
        int maximumNumber = randomNumbers.stream().max(Integer::compare).get();
        System.out.println("Maxium element in the list " + maximumNumber);
        int minimumNumber = randomNumbers.stream().min(Integer::compare).get();
        System.out.println("Minimum element in the list " + minimumNumber);
    }
}