/*
Requirement:
     Sort the roster list based on the person's age in descending order using java.util.Stream
     
Entity:
     SortPersonDescending
     Person
     
Function Declaration:
     public static void main(String[] args) {}

Jobs To Be Done:
     1) Create an object as roster for the method createRoster().
     2) Sort the person in descending order using stream based on the person age .
     3) Print the person using stream for each loop.
     
Pseudo code:
	public class SortPersonDescending {
	
		public static void main(String[] args) {
		    List<Person> roster = Person.createRoster();
		    //sort the roster using stream.
		    Stream<Person> stream = newRoster.stream();
		    stream.forEach(person -> System.out.println(person.name + " " + person.birthday + " "
		            + person.gender + " " + person.emailAddress));
		}
	} 
*/

package com.kpr.training.stream;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class SortAge {

    public static void main(String[] args) {
    	
        List<Person> roster = Person.createRoster();
        
        List<Person> newRoster = roster.stream()
        							   .sorted(Comparator.comparing(person -> person.getAge()))
        							   .collect(Collectors.toList());
        
        Stream<Person> stream = newRoster.stream();
        
        stream.forEach(person -> System.out.println(person.name + "\t" + person.birthday + "\t"
                + person.gender + "\t" + person.emailAddress + "\t" + person.getAge()));
    }
}