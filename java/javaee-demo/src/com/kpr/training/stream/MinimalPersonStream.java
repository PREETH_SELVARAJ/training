/*
 * Requirement:
 *      To print minimal person with name and email address from the Person class using java.util.Stream<T> API by referring Person.java
 * 
 * Entity:
 *      MinimalPersonStream
 *      Person
 *
 * Function Declaration:
 *      public static void main(String[] args)
 *
 * Jobs To Be Done:
 *      1)Invoke the method createRoater from Person and store it in list.
 *      2)Create a ArrayList named name of type String.
 *      3)Create a ArrayList named mailId of type String.
 *      4)For each person
 *        4.1)Invoke the method getName from Person and add to the list name.
 *        4.2)Invoke the method getEmailAddress from Person and add to the list mailId.
 *     5)Declare a variable of type string named minimalName and store the minimum name.
 *     6)Declare a variable of type string named minimalId and store the minimum mailId.
 *     7)Print the minimalName.
 *     8)Print the minimalId.
 * Pseudo code:
 * 
 * public class MinimalPersonStream {
    
    public static void main(String[] args) {
        List<Person> list = Person.createRoster();
        ArrayList<String> name = new ArrayList<>();
        ArrayList<String> mailId = new ArrayList<>(); 
        for (Person p : list) {
               //get name 
               //get mailId
        }
        String minimalName = Collections.min(name);
        String minimalId = Collections.min(mailId);

        System.out.println(
                "The Minimal Person Name is " + minimalName + " And EmailId is " + minimalId);
    }

}
        
 */
package com.kpr.training.stream;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class MinimalPersonStream {
    
    public static <R, T> void main(String[] args) {
        List<Person> list = Person.createRoster();
        ArrayList<String> name = new ArrayList<>();
        ArrayList<String> mailId = new ArrayList<>();
        for (Person person : list) {
            name.add(person.getName());
            mailId.add(person.getEmailAddress());
        }
        String minimalName = Collections.min(name);
        String minimalId = Collections.min(mailId);

        System.out.println(
                "The Minimal Person Name is " + minimalName + " And EmailId is " + minimalId);
    }
   

}
