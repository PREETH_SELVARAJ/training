/*
 * Requirement:
 *      To filter the Person, who are male and
        - find the first person from the filtered persons
        - find the last person from the filtered persons
        - find random person from the filtered persons

    Entity:
        FilterMale
        Person
    
    Function Declaration:
        public static void main(String[] args)
        
    Jobs To Be Done:
        1)Invoke the method createRoater from Person and store it in list.
 *      2)Create a ArrayList named filteredList of type String.
 *          2.1)For each person in the list stream
 *             2.1.1)Filter the person who are male
 *             2.1.2)Map the name of the filtered person
 *             2.1.3)Collect the mapped person  as list and store it in filteredList.
 *      3)Print the first person in the filteredList.
 *      4)Print the last person in the filteredList.
 *      5)Print the random person in the filteredList.
 
  Pseudocode:
  
public class FilterMale {

    public static void main(String[] args) {
        
    	List<Person> list = Person.createRoster();
        ArrayList<String> filteredList = (ArrayList<String>)list.stream()
            //Filter the person who are male
            //Map the name of the filtered person
            .collect(Collectors.toList());
        int size = filteredList.size();
        System.out.println("The First Filtered Person is " + filteredList.get(0));
        System.out.println("The Last Filtered Person is " + filteredList.get(size - 1));

        Random random = new Random();
        int index = random.nextInt(size);
        System.out.println("The Random Filtered Person is " + filteredList.get(index));

    }
}
               
*/
package com.kpr.training.stream;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

public class FilterMale {

    public static void main(String[] args) {
        
    	List<Person> list = Person.createRoster();
        ArrayList<String> filteredList = (ArrayList<String>)list.stream()
            .filter(person -> (person.getGender() == Person.Sex.MALE ))
            .map(name -> name.getName())
            .collect(Collectors.toList());
        int size = filteredList.size();
        System.out.println("The First Filtered Person is " + filteredList.get(0));
        System.out.println("The Last Filtered Person is " + filteredList.get(size - 1));

        Random random = new Random();
        int index = random.nextInt(size);
        System.out.println("The Random Filtered Person is " + filteredList.get(index));

    }
}
