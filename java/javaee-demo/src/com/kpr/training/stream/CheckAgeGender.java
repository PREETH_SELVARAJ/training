/*Requirement:
 * 		Write a program to filter the Person, who are male and age greater than 21
 * 
 * Entity:
 *		CheckAgeGender
 *      Person
 *
 *Function Declaration:
 *		public static void main(String[] args) { }
 *      public static List<Person> createRoster() { }
 *      public Sex getGender() { }
 *      public int getAge() { }
 *      
 *Jobs To Be Done:
 *		1)Invoke the method createRoaster from Person and store it in list.
 *      2)For each person
 *        2.1)Check if persons gender is male and persons age is greater than 21.
 *        2.2)Invoke method getName from Person and print the name.
 *        
 *pseudo code:
 	public class CheckAgeGender {
	 
		public static void main(String[] args) {
	       
		    List<Person> list = Person.createRoster();  
	        
	        use stream iterate the list.
	        filter(Gender == Male && Age >21 )
	        print the filtered person name
	        
	   }
    }
*/
package com.kpr.training.stream;

import java.util.List;

public class CheckAgeGender {
	 
	public static void main(String[] args) {
	       
		    List<Person> list = Person.createRoster();  
	        
	        list.stream()
	             .filter(person -> (person.getGender() == Person.Sex.MALE ))
	             .filter(person -> person.getAge() > 21)
	             .forEach(print -> System.out.println(print.getName()));
	}
}
