/*
Requirement:Create a stack using generic type and implement
                   -> Push atleast 5 elements
                   -> Pop the peek element
                   -> search a element in stack and print index value
                   -> print the size of stack
                   -> print the elements using Stream
   
 Entity:StackDemo
 
 Function Declaration:public static void main(String[] args)
 
 Jobs To Be Done:1.A stack is created named stack
                 2.Elements are pushed into the stack
                 4.Print the peek element i.e last element
                 5.Index of the particular element is searched and that index is printed.
                 6.The size of the stack is printed.
                 7.Create a Stream named stream which store stream of stack elements.
                 8.For each element in stream
                   8.1.Print the element.
                   
 pseudo code:
 
 public class StackDemo {
	
	    public static void main(String[] args) {
	    	
	    	Stack<Integer> stack = new Stack<Integer>();
	    	
	    	//elements pushed into the stack
	   
	        System.out.println(stack.pop());
	       
	       
	        System.out.println(stack.search(2));
	
	        System.out.println(stack.size());
	       
          Stream<Integer> stream = stack.stream();
	        stream.forEach((value) -> System.out.println(value));
	    	
	    }

 }
   
 */

package com.kpr.training.stack;

import java.util.Stack;
import java.util.stream.Stream;

public class StackDemo {
	
	public static void main(String[] args) {
		
		Stack<Integer> stack = new Stack<Integer>();
		
		//elements pushed into the stack
		for(int number = 0; number < 5; number++) { 
            stack.push(number); 
        }
		
		//pop the peek element
	    System.out.println("pop operation:"+stack.pop());
	   
	    //print index value of the element
	    System.out.println("index of element 2:"+stack.search(2));
	   
	    //size of the stack
	    System.out.println("Lenght of the stack:"+stack.size());
	   
	    //printing elements using stream
	    System.out.println("Printing stack elements using stream:");
        Stream<Integer> stream = stack.stream();
	    stream.forEach((value) -> System.out.println(value));
		
	}
}

