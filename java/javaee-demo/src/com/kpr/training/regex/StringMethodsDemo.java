/*Requirement:
 * 		write a program for Java String Regex Methods?
 * 
 * Entity:
 * 		StringMethodDemo
 * 
 * Function Declaration:
 * 		public static void main(String[] args)
 * 
 * Jobs To Be Done:
 * 		1)Declare a String named string and store a string "Betty bought some butter the butter was 
 *      bitter"in it.
 * 		2)check whether the string matches the "some" and store the result as boolean in the variable
 *        match
 *      3)Check whether the match is true.
 *        3.1)Print as "Match found"
 *        3.2)Otherwise print "Match not found".
 *      4)Split the string where the butter occurs and store in string array named array.
 *      5)print the string array.
 *      6)Replace butter by bitter at the first occurrence in the string and store in word
 *      7)print the word.
 *      8)Replace all the butter in the string by bitter and store it in sentence.
 *      9)print the sentence.
 * 
 *pseudo code:
public class StringMethodsDemo {
	
	public static void main(String[] args) {
		
		String string = "Betty bought some butter the butter was bitter";
		boolean match = string.matches("some") ;
		if(match) {
			System.out.println("Match found") ;
		} else {
			System.out.println("Match not found");
		}
		String[] array = string.split("butter");
		System.out.println(Arrays.toString(array));
		
		String word = string.replaceFirst("butter","bitter");
		System.out.println(word);
		
		String sentence  = string.replaceAll("butter","bitter");
		System.out.println(sentence);
		
	}
} *      
 * 
 */
package com.kpr.training.regex;

import java.util.Arrays;

public class StringMethodsDemo {
	
	public static void main(String[] args) {
		
		String string = "Betty bought some butter the butter was bitter";
		boolean match = string.matches("some") ;
		if(match) {
			System.out.println("Match found") ;
		} else {
			System.out.println("Match not found");
		}
		String[] array = string.split("butter");
		System.out.println(Arrays.toString(array));
		
		String word = string.replaceFirst("butter","bitter");
		System.out.println(word);
		
		String sentence  = string.replaceAll("butter","bitter");
		System.out.println(sentence);
		
	}
}
