/*
Requirement:
    write a program for email-validation?

Entity:
    EmailValidation

Function Declaration :
    public static boolean validMail(String mailid)
    public static void main(String[] args) 

Jobs To Be Done:
    1) Create an object for the scanner
    2) Get mail Id From user and store it in mailId.
    3) Invoke the validMail method From the class EmailValidationDemo 
      3.1) Pass the user mailId as parameter
      3.2) Declare a pattern for mailid and store it in pattern
      3.3) Create a Matcher named matcher and store the match of the pattern against the id.
      3.4) If the pattern it will return true otherwise it will return false.
    4) Returned value is printed.

pseudo code:

public class EmailValidation {
    
    public static boolean validMail(String id) {
     
        Declare a pattern for mailid and store it in pattern    
    
        Matcher matcher = pattern.matcher(id);
        return matcher.matches();
    }

    public static void main(String[] args) {
        
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter the mailid ");
        String mailId = scanner.next();
        System.out.println(validMail(mailId));
        scanner.close();
    }
}

 */
package com.kpr.training.regex;

import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class EmailValidation {
    
    public static boolean validMail(String id) {
     
        Pattern pattern = Pattern.compile("^[a-zA-Z0-9_+&*-]+(?:\\."+  "[a-zA-Z0-9_+&*-]+)*@" 
                                          + "(?:[a-zA-Z0-9-]+\\.)+[a-z" + "A-Z]{2,7}$");

        Matcher matcher = pattern.matcher(id);
        return matcher.matches();
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter the mailid ");
        String mailId = scanner.next();
        System.out.println("Email valid :"+validMail(mailId));
        scanner.close();
    }
}
