/*Requirement:*catching multiple exception with example. 
 *            *Is it possible to have more than one try block?
 *
 *Entity:MultipleCatch
 *
 *Function Declaration:public static void main(String[] args)
 *
  * Jobs to be Done:1)An Array is created name array and elements are stored in it.
 *                 2)For each element
 *                   2.1)Two is divided by the element at the each index.
 *                   2.1.1)Throws an exception and print as Array index out of range.
 *                   2.1.2)Throws an exception and print as Exception is handled.  
 *                   
 *pseudo code:

public class MultipleCatch {
	
	public static void main(String[] args) {
       
		int[] array = new int[] {1,2,3,4,5};
		for(int i = 0; i <= 5; i++) {
			
		    try {
			    System.out.println(2/array[i]);
			} catch(ArrayIndexOutOfBoundsException e) {
				System.out.println("Array index out of range");
		    } catch(Exception e) {
				System.out.println("Exception is handled");
		    }
		} 
	}
}

Reason:No each Try block must be handled using catch block
 *       If you use multiple try block for many catch it will generate an error during compile 
 *       time.
 * 
 */

package com.kpr.training.exception;

public class MultipleCatch {
	
	public static void main(String[] args) {
       
		int[] array = new int[] {1,2,3,4,5};
		for(int i = 0; i <= 5; i++) {
			
		    try {
			    System.out.println(2/array[i]);
			} catch(ArrayIndexOutOfBoundsException e) {
				System.out.println("Array index out of range");
		    } catch(Exception e) {
				System.out.println("Exception is handled");
		    }
		} 
	}
}
