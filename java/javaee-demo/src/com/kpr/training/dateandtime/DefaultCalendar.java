/*
Requirement:
     Write a Java program to get and display information (year, month, day, hour, minute) of a 
     default calendar
    
Entity:
    DefaultCalendar
    
Function Declaration:
    public static void main(String[] args)
    
Jobs to be done:
    1)Create a object for Calendar named calendar and the calendar is stored in it for the loacl time
      zone.
    2)The year is printed.
    3)The month is printed.
    4)The day is printed.
    5)The hour is printed.
    6)The minute is printed.
    
Pseudo code:

public class DefaultCalendar {
    
 
    public static void main(String[] args) {
        Calendar calendar = Calendar.getInstance();
        System.out.println("Year: " + " " + calendar.get(Calendar.YEAR));
        System.out.println("Month: " + " " + calendar.get(Calendar.MONTH));
        System.out.println("Date: " + " " + calendar.get(Calendar.DATE));
        System.out.println("Hour: " + " " + calendar.get(Calendar.HOUR));
        System.out.println("Minute: " + " " + calendar.get(Calendar.MINUTE));
    }
}
*/

package com.kpr.training.dateandtime;

import java.util.Calendar;

public class DefaultCalendar {
    
 
    public static void main(String[] args) {
        Calendar calendar = Calendar.getInstance();
        System.out.println("Year: " + " " + calendar.get(Calendar.YEAR));
        System.out.println("Month: " + " " + calendar.get(Calendar.MONTH));
        System.out.println("Date: " + " " + calendar.get(Calendar.DATE));
        System.out.println("Hour: " + " " + calendar.get(Calendar.HOUR));
        System.out.println("Minute: " + " " + calendar.get(Calendar.MINUTE));
    }
}