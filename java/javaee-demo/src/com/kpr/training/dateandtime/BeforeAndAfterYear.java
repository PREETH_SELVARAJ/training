 /*

Requirement:
    Write a Java program to get a date before and after 1 year compares to the current date.       
Entity:
    BeforeAndAfterYear
    
Function Declaration:
    public static void main(String[] args)
    
Jobs to be done:
    1)An instance is created for Calendar as calendar and gets the calendar for current time zone
       and store it.
    2)Print the current date
    3)Subtract a year from the current date.
    4)Print the date before an year from the current date.
    5)Add two years to the calendar.
    6)Print the date after an year from the current date.
    
Pseudo code:

public class BeforeAndAfterYear {
    
    public static void main(String[] args) {
        
    	Calendar calender = Calendar.getInstance();
        System.out.println("The current date is " + calender.getTime());
        
        calender.add(Calendar.YEAR, -1);
        System.out.println("Date before 1 year is " + calender.getTime());
        
        calender.add(Calendar.YEAR, 2);
        System.out.println("The date after 1 year after the current day is " + calender.getTime());
    }
}


*/

package com.kpr.training.dateandtime;

import java.util.Calendar;

public class BeforeAndAfterYear {
    
    public static void main(String[] args) {
        
    	Calendar calender = Calendar.getInstance();
        System.out.println("The current date is " + calender.getTime());
        
        calender.add(Calendar.YEAR, -1);
        System.out.println("Date before 1 year is " + calender.getTime());
        
        calender.add(Calendar.YEAR, 2);
        System.out.println("The date after 1 year after the current day is " + calender.getTime());
    }
}
