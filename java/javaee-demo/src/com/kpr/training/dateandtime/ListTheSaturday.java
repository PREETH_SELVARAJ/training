/*Requirement: To find the previous friday when given with a date.
 * 
 * Entity: ListTheSaturday
 * 
 * Function Declaration: public static void main(String[] args);
 * 
 * Jobs to be done:
 * 		1. Assign the input month to month variable which is of type Month.
 * 		2. Find the first Saturday of the month and store it to date.
 * 		3. While the month1 equals month.
 * 			4.1) Print the date of saturday.
 * 			4.2) Change the value of the date to the next Saturday of month.
 * 
 * PseudoCode:

public class ListTheSaturday {

	public static void main(String[] args) {
        
		Month month = Month.of(10);

        System.out.printf("For the month of %s:%n", month);
        LocalDate date = Year.now().atMonth(month).atDay(1)
        					 .with(TemporalAdjusters.firstInMonth(DayOfWeek.SATURDAY));
        
        while (date.getMonth() == month) {
        	
            System.out.printf("%s%n", date);
            date = date.with(TemporalAdjusters.next(DayOfWeek.SATURDAY));
        }
    }
}

 * */
package com.kpr.training.dateandtime;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.Month;
import java.time.Year;
import java.time.temporal.TemporalAdjusters;

public class ListTheSaturday {

	public static void main(String[] args) {
        
		Month month = Month.of(10);

        System.out.printf("For the month of %s:%n", month);
        LocalDate date = Year.now().atMonth(month).atDay(1)
        					 .with(TemporalAdjusters.firstInMonth(DayOfWeek.SATURDAY));
        
        while (date.getMonth() == month) {
        	
            System.out.printf("%s%n", date);
            date = date.with(TemporalAdjusters.next(DayOfWeek.SATURDAY));
        }
    }
}
