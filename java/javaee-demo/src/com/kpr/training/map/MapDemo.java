/*Requirement:* Write a Java program to copy all of the mappings from the specified map to another
                map?
              * Write a Java program to test if a map contains a mapping for the specified key?
              * Count the size of mappings in a map

 Entity: MapDemo
 
 Function Declaration:public static void main(String[] args)
 
 Jobs To Be Done:1.Hash map is  created named hash_map1.
                 2.Hash map is  created named hash_map2.
                 3.The values are inserted into hash_map1.
                 4.hash_map1 is printed.
                 5.All the elements of hash_map1 is inserted to hash_map2 
                 6.hash_map2 is printed.
                 5.Check whether the key 10 is present in the hash_map1
                   5.1)Print Yes key is present
                   5.2)otherwise print as No key is not present.
                 7.Print the size of the hash_map1.
                 
pseudo code:
public class MapDemo {
	
	public static void main(String[] args) {
		
		  HashMap <Integer,Integer> hash_map1 = new HashMap <Integer,Integer> ();
		  HashMap <Integer,Integer> hash_map2 = new HashMap <Integer,Integer> ();
		  
		  //Insert the values into the hash_map1
		   
		  System.out.println(hash_map1);
		  
		  
		  hash_map2.putAll(hash_map1);
		  System.out.println( hash_map2);
		
		  if (hash_map1.containsKey(10)) {
			  System.out.println("Yes key is present");
		  } else {
			  System.out.println("key is not  present");
		  }
		  
		  
		  System.out.println(hash_map1.size());	  
		
	}

}

 */

package com.kpr.training.map;

import java.util.HashMap;

public class MapDemo {
	
	public static void main(String[] args) {
		
		  HashMap <Integer,Integer> hash_map1 = new HashMap <Integer,Integer> ();
		  HashMap <Integer,Integer> hash_map2 = new HashMap <Integer,Integer> ();
		  hash_map1.put(1,10);
		  hash_map1.put(2,20);
		  hash_map1.put(3, 30);
		  System.out.println("first map values: " + hash_map1);
		  
		  //copy first map to second map
		  hash_map2.putAll(hash_map1);
		  System.out.println("After copying values of first map to second map: " + hash_map2);
		  
		  //to check key is present or not
		  if (hash_map1.containsKey(10)) {
			  System.out.println("Yes key is present");
		  } else {
			  System.out.println("key is not  present");
		  }
		  
		  //count the size of the map
		  System.out.println("Size of the hash map: "+hash_map1.size());	  
		
	}

}
