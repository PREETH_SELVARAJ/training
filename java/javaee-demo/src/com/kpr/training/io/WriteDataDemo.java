/*Requirement: To write data to multiple files together using bytearray and outputstream.
 * 
 * Entity:WriteDataDemo
 * 
 * Method Signature: public static void main(String[] args)
 * 
 * Jobs to be done: 1. Create OutputStreams for file named file1.
 *                  2. Create OutputStreams for file named file2.
 * 					3. Initialize the string value to be written in the file to string.
 * 					4. Convert the string to byte array named bytes.
 * 					5. Write the bytes to file1 using OutputStream.
 *                  6. Write the bytes to file2 using OutputStream.
 * PseudoCode:

public class WriteDataDemo {
	
	public static void main(String[] args) throws IOException {
		
		OutputStream outputStream = new FileOutputStream("C:\\1dev\\training\\java\\javaee-demo\\file1.txt");
		OutputStream outputStream1 = new FileOutputStream("C:\\1dev\\training\\java\\javaee-demo\\file2.txt");
		String string = "Write" + "Data" + "Demo"; 

		byte[] bytes = string.getBytes(); 
		outputStream.write(bytes);
		outputStream1.write(bytes);
		System.out.println("Written successfully");
		outputStream.close();
		outputStream1.close();
	}
}


 */
package com.kpr.training.io;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;

public class WriteDataDemo {
	
	public static void main(String[] args) throws IOException {
		
		OutputStream outputStream = new FileOutputStream("C:\\1dev\\training\\java\\javaee-demo\\file1.txt");
		OutputStream outputStream1 = new FileOutputStream("C:\\1dev\\training\\java\\javaee-demo\\file2.txt");
		String string = "Write" + "Data" + "Demo"; 

		byte[] bytes = string.getBytes(); 
		outputStream.write(bytes);
		outputStream1.write(bytes);
		System.out.println("Written successfully");
		outputStream.close();
		outputStream1.close();
	}
}