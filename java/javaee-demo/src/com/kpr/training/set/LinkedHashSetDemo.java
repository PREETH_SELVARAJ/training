/*
Requirement:
    demonstrate program explaining basic add and traversal operation of linked hash set

Entity:
    LinkedHashSetDemo

Function Declaration:
    public static void main(String[] args){} 

Jobs To Be Done:
    1)Create a LinkedHashSet named linkedHashSet.
    2)Add elements to the linkedHashSet
    3)create a iterator and store the linkedHashset in it.
    4)Iterate the LinkedHashSet
        4.1)print the element in the linkedHashSet
    
Pseudo code:
    public class LinkedHashSetDemo {
        
        public static void main(String[] args) {
           
            LinkedHashSet<String> linkedHashSet = new LinkedHashSet<String>();
            //add elements to the linkedhashset
            Iterator<String> iterator = new Iterstor<String>();
            while(iterator.hasNext()){
            
                 System.out.Pritln(print the element) ;
            }
        }
    }
*/
package com.kpr.training.set;

import java.util.Iterator;
import java.util.LinkedHashSet;

public class LinkedHashSetDemo {

	public static void main(String[] args) {

		LinkedHashSet<String> linkedHashSet = new LinkedHashSet<String>();

		linkedHashSet.add("apple");
		linkedHashSet.add("banana");
		linkedHashSet.add("cap");
		linkedHashSet.add("deer");
		
		System.out.println(linkedHashSet);

		Iterator<String> iterator = linkedHashSet.iterator();

		while(iterator.hasNext()) {
			System.out.println(iterator.next());;
		}
	}
}

