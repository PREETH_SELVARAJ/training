/*Requirement:
 *   demonstrate linked hash set to array() method in java
 * 
 * Entity:
 *   ToArrayDemo
 *   
 *Function Declaration:
 *   public static void main(String[] args){ }
 *   
 *Jobs To Be Done:
 *    1)Create a LinkedHashset named linkedHashSet.
 *    2)Add elements o the linkedHashSet
 *    3)create a string array named array
 *    4)Convert the linkedHashSet to array and store in array
 *    5)For each element in the array
 *      5.1)Print the element.
 *       
 * 
 *  pseudo code:
 *    public class ToArrayDemo {
        
        public static void main(String[] args) {
           
            LinkedHashSet<String> linkedHashSet = new LinkedHashSet<String>();
            //add elements to the linkedHashSet
            String[] array = new String[];
            array = linkedHashSet.toarray(array);
            for(int number = 0 ;number < arr.length ;number++) {
			       System.out.println(array[i]);
	      	}
        }       
    }
 * 
 */
package com.kpr.training.set;

import java.util.LinkedHashSet;

public class ToArrayDemo {
	
public static void main(String[] args) {
		
		LinkedHashSet<String> linkedHashSet = new LinkedHashSet<String>();
		
		linkedHashSet.add("apple");
		linkedHashSet.add("banana");
		linkedHashSet.add("carrot");
		linkedHashSet.add("dolphin");
		
		System.out.println(linkedHashSet);
		
		String[] array = new String[4];
		array = linkedHashSet.toArray(array);
		
		for(int number = 0 ;number < array.length ;number++) {
			System.out.println(array[number] );
		}
	}
}
