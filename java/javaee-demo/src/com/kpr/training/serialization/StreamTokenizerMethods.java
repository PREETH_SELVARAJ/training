package com.kpr.training.serialization;
/*
 * Requirement:
 *    Mention any five methods of stream tokenizer?
 *Solution:
 *  Stream Tokenizer methods:
 *      void commentChar(int ch)
 *      void eolIsSignificant(boolean flag)
 *      int lineno()
 *      void lowerCaseMode(boolean fl)
 *      int nextToken()
 */
